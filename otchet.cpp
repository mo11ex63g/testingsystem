#include "otchet.h"
#include "ui_otchet.h"

Otchet::Otchet(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::Otchet)
{
    ui->setupUi(this);
    ui->tableWidget->setColumnCount(5);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "Тема" << "Результат" << "Баллы" << "Дата" << "Врнмя");
}

Otchet::~Otchet()
{
    delete ui;
}

void Otchet::AddRow(QString Tema, QString ResultText, QString Ball, QString Data, QString Time)
{
    ui->tableWidget->insertRow(ui->tableWidget->rowCount());
    for(int i = 0; i <ui->tableWidget->rowCount(); i++)
    {
        if(i == ui->tableWidget->rowCount() - 1)
        {
            ui->tableWidget->setItem(i, 0, new QTableWidgetItem(Tema));
            ui->tableWidget->setItem(i, 1, new QTableWidgetItem(ResultText));
            ui->tableWidget->setItem(i, 2, new QTableWidgetItem(Ball));
            ui->tableWidget->setItem(i, 3, new QTableWidgetItem(Data));
            ui->tableWidget->setItem(i, 4, new QTableWidgetItem(Time));
        }
    }
}

void Otchet::on_pushButton_clicked()
{
    close();
}

