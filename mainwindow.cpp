#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    RegUi = new Registration;
    MainMenu = new menu;
    user = new userinfo;
    temaChoice = new ChoiceTema;
    test = new TestWindow;
    otchet = new Otchet;

    connect(RegUi, &Registration::SignalReg, this, &MainWindow::SlotReg);
    connect(RegUi, &Registration::SignalShow, this, &MainWindow::SlotShow);
    connect(MainMenu, &menu::ShowQuestion, this, &MainWindow::SlotShowQuestion);
    connect(MainMenu, &menu::ShowUserInfo, this, &MainWindow::SlotShowUserInfo);
    connect(temaChoice, &ChoiceTema::GoToTest, this, &MainWindow::SlotTest);
    connect(test, &TestWindow::NextTest, this, &MainWindow::SlotNextQuest);
    connect(MainMenu, &menu::ShowOtchet, this, &MainWindow::SlotShowOtchet);
    connect(test, &TestWindow::LastQuestion, this, &MainWindow::SlotSetOtchet);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::ConnectToBD()
{
    qDebug() << QSqlDatabase::drivers();
    qDebug("conection....");
    //Подключение к базе данных MySql
    database = QSqlDatabase::addDatabase("QMYSQL");
    database.setHostName("localhost");
    database.setUserName("root");
    database.setPassword("root");
    database.setDatabaseName("testsystem");
}

void MainWindow::on_EnterButtom_clicked()
{
    ConnectToBD();

    QString Login = ui->LoginLine->text();
    QString Password = ui->PasswordLine->text();

    if(database.open())
    {
        QSqlQuery query(QSqlDatabase::database("MyConnect"));

        query.prepare(QString("SELECT * FROM user WHERE Login = :Login AND Password = :Password"));

        query.bindValue(":Login", Login);
        query.bindValue(":Password", Password);

        if(!query.exec())
        {
            QMessageBox::information(this, "Failed", "Query Failed to execute");
        }
        else
        {
            while(query.next())
            {
                QString LoginFromBD = query.value(5).toString();
                QString PasswordFromBD = query.value(6).toString();

                if(LoginFromBD == Login && PasswordFromBD == Password)
                {
                    ui->statusbar->showMessage("Авторизация пройдена успешно", 3000);

                    FirstName = query.value(1).toString();
                    SecondName = query.value(2).toString();
                    NumberPhone = query.value(4).toString();
                    DateBirthday = query.value(3).toString();

                    user->SetUserID(query.value(0).toString());

                    MainMenu->show();
                    close();
                }
                else
                {
                    QMessageBox::warning(this, "Failed", "Login Failed");
                    ui->statusbar->showMessage("Авторизация не пройдена", 3000);
                }
            }
        }
    }
    else
    {
        QMessageBox::warning(this, "Not Connected", "Database is not connected!");
    }
}


void MainWindow::on_RegButton_clicked()
{
    RegUi->show();
    close();
}

void MainWindow::SlotReg(QString RFirstName, QString RSecondName, QString RNumberPhone, QString RegLogin, QString RegPass, QString Date)
{

    ConnectToBD();

    if(database.open())
    {
        QMessageBox::information(this, "Connected", "Database is connected!");

        //Получаем введённые данные с окна регистрации
        QString FirstName = RFirstName;
        QString SecondName = RSecondName;
        QString NumberPhone = RNumberPhone;
        QString Login = RegLogin;
        QString Password = RegPass;
        QString DateBirthday = Date;

        //Выполним наш запрос на вставку
        QSqlQuery qry;
        qry.prepare("INSERT INTO user (FirstName, SecondName, DateBirthday, NumberPhone, Login, Password)"
                    "VALUES (:FirstName, :SecondName, :DateBirthday, :NumberPhone, :Login, :Password)");

        qry.bindValue(":FirstName", FirstName);
        qry.bindValue(":SecondName", SecondName);
        qry.bindValue(":NumberPhone", NumberPhone);
        qry.bindValue(":Login", Login);
        qry.bindValue(":Password", Password);
        qry.bindValue(":DateBirthday", DateBirthday);

        if(qry.exec())
        {
            QMessageBox::information(this, "Inserted", "Data inserted successfully");
        }
        else
        {
            QMessageBox::information(this, "Not Inserted", "Data is not inserted");
        }
    }
    else
    {
        QMessageBox::warning(this, "Not Connected", "Database is not connected!");
    }
}

void MainWindow::SlotShowUserInfo()
{
    user->show();
    user->ShowInfo(FirstName, SecondName, NumberPhone, DateBirthday);
}

void MainWindow::SlotShowQuestion()
{
    temaChoice->show();

    //ConnectToBD();
    if(!bIsDo)
    {
        if(database.open())
        {
            QSqlQuery query(QSqlDatabase::database("MyConnect"));
            query.prepare(QString("SELECT * FROM tema"));
            if(!query.exec())
            {
                QMessageBox::information(this, "Failed", "Query Failed to execute");
            }
            else
            {
                bIsDo = true;
                while(query.next())
                {
                    temaChoice->AddToBox(query.value(1).toString());
                }
            }
        }
    }
}

void MainWindow::SlotTest()
{
    //ConnectToBD();

    if(database.open())
    {
        test->Voprosi.clear();
        test->Answers.clear();
        test->TrueAnswers.clear();
        test->Result = 0.0f;
        QSqlQuery query(QSqlDatabase::database("question"));
        query.prepare(QString("SELECT * FROM vopros WHERE Tema_Parent = :IdTema"));
        query.bindValue(":IdTema", temaChoice->CurrentIndexTema);
        if(!query.exec())
        {
            QMessageBox::information(this, "Failed", "Query Failed to execute");
        }
        else
        {
            while(query.next())
            {
                QString Vopros = query.value(1).toString();
                int IdVopr = query.value(0).toInt();
                test->Voprosi.insert(IdVopr, Vopros);
                QSqlQuery qry(QSqlDatabase::database("answer"));
                qry.prepare(QString("SELECT * FROM otvet WHERE Parent_ID = :IdVopr"));
                qry.bindValue(":IdVopr", IdVopr);
                if(!qry.exec())
                {
                    QMessageBox::information(this, "Failed", "Query Failed to execute");
                }
                else
                {
                    while(qry.next())
                    {
                        QPair<int, QString> Answer;
                        Answer.first = qry.value(0).toInt();
                        Answer.second = qry.value(1).toString();
                        test->Answers.append(Answer);
                        if(qry.value(2).toInt() == 1)
                        {
                            test->TrueAnswers.append(qry.value(1).toString());
                        }
                    }
                    //qDebug() << query.record();
                }
            }
        }
    }

    test->CurrentVopros = test->Voprosi.begin();
    if(test->Voprosi.empty())
    {
        qDebug("No question");
    }
    else
    {
        test->UpdateInfo(test->CurrentVopros.key());
        test->show();
    }
}

void MainWindow::SlotNextQuest()
{
    Wait = false;
}

void MainWindow::SlotShowOtchet()
{
    otchet->show();
    if(!bIsDo2)
    {
        if(database.open())
        {
            QSqlQuery qry(QSqlDatabase::database("otchet"));
            qry.prepare("SELECT * FROM otchet WHERE User_ID = :User_ID");
            qry.bindValue(":User_ID", user->GetUserID());
            if(qry.exec())
            {
                while(qry.next())
                {
                    QSqlQuery query(QSqlDatabase::database("tema"));
                    query.prepare("SELECT Tema_text FROM tema WHERE Tema_ID = :Tema_ID");
                    query.bindValue(":Tema_ID", qry.value(3).toInt());
                    if(query.exec())
                    {
                        while(query.next())
                        {
                            otchet->AddRow(query.value(0).toString(), qry.value(1).toString(), qry.value(2).toString(), qry.value(4).toString(), qry.value(5).toString());
                        }
                    }
                }
            }
            else
            {
                QMessageBox::information(this, "Not open", "Data is not qry");
            }
        }
        bIsDo2 = true;
    }

}

void MainWindow::SlotSetOtchet()
{
    if(database.open())
    {
        int SecTime = test->EndTime.second() - temaChoice->StartTime.second();

        QTime Time = QTime(0, 0).addSecs(SecTime);

        QSqlQuery qry;
        qry.prepare("INSERT INTO otchet (User_ID, Res_text, Ball, Tema_ID, Date, Time)"
                    "VALUES (:User_ID, :Res_text, :Ball, :Tema_ID, :Date, :Time)");

        qry.bindValue(":User_ID", user->GetUserID());
        qry.bindValue(":Res_text", test->ResultText);
        qry.bindValue(":Ball", test->Result);
        qry.bindValue(":Tema_ID", temaChoice->CurrentIndexTema);
        qry.bindValue(":Date", QDate::currentDate());
        qry.bindValue(":Time", Time.toString("hh:mm:ss"));

        if(qry.exec())
        {
            //QMessageBox::information(this, "Inserted", "Data inserted successfully");
        }
        else
        {
            QMessageBox::information(this, "Not Inserted", "Data is not inserted");
        }
    }
    else
    {
        QMessageBox::warning(this, "Not Connected", "Database is not connected!");
    }
    bIsDo2 = false;
}

void MainWindow::SlotShow()
{
    show();
}
