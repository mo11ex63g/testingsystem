#include "testwindow.h"
#include "ui_testwindow.h"
#include <iostream>

TestWindow::TestWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TestWindow)
{
    ui->setupUi(this);
    qDebug() << "OPOPOP";
    //FillInfo(Voprosi.find(1).value(), Answers.value(0).second, Answers.value(1).second, Answers.value(2).second, Answers.value(3).second);
}

TestWindow::~TestWindow()
{
    delete ui;
}

void TestWindow::FillInfo(QString Question, QString one, QString two, QString three, QString four)
{
    ui->label->setText(Question);
    ui->checkBox_1->setText(one);
    ui->checkBox_2->setText(two);
    ui->checkBox_3->setText(three);
    ui->checkBox_4->setText(four);
}

void TestWindow::UpdateInfo(int Idvopr)
{
    QList<QString> CurrentAnswers;
    for (const auto &it : Answers)
    {
        if(it.first == Idvopr) { CurrentAnswers.append(it.second); }
    }
    FillInfo(Voprosi.find(Idvopr).value(), CurrentAnswers.at(0), CurrentAnswers.at(1), CurrentAnswers.at(2), CurrentAnswers.at(3));
}

void TestWindow::AddBalls(QVector<QString> Otvets)
{
    float Balls = 0.0f;
    for(QString i : TrueAnswers)
    {
        for(QString j : Otvets)
        {
            if(i == j)
            {
                Balls += 1.0f / Otvets.count();
            }
        }
    }
    Result += Balls;
}

void TestWindow::on_pushButton_clicked()
{
    QVector<QString> Otvets;
    int TheEnd;

    for(auto child : findChildren<QCheckBox*>())
    {
        //qDebug() << child->checkState();
        if(child->checkState())
        {
            Otvets.append(child->text());
        }
    }

    AddBalls(Otvets);

    for(auto i = Voprosi.begin(), end = Voprosi.end(); i != end; ++i)
    {
        TheEnd = i.key();
    }

    if(CurrentVopros.key() == TheEnd)
    {
        EndTime = QTime::currentTime();
        qDebug("Last question");
        qDebug() << Result;
        if(Result / Voprosi.count() >= 0.85f) { ResultText = "Отлично"; }
        else if(Result / Voprosi.count() < 0.85f && Result / Voprosi.count() > 0.6f) { ResultText = "Хорошо"; }
        else if(Result / Voprosi.count() < 0.6f && Result / Voprosi.count() > 0.4f) { ResultText = "Удовлетворительно"; }
        else { ResultText = "Не успешно"; }

        emit LastQuestion();
        close();
    }
    else
    {
        CurrentVopros++;
        UpdateInfo(CurrentVopros.key());
    }

    ui->checkBox_1->setCheckState(Qt::CheckState::Unchecked);
    ui->checkBox_2->setCheckState(Qt::CheckState::Unchecked);
    ui->checkBox_3->setCheckState(Qt::CheckState::Unchecked);
    ui->checkBox_4->setCheckState(Qt::CheckState::Unchecked);

    //qDebug() << TrueAnswers;
}
