#ifndef TESTWINDOW_H
#define TESTWINDOW_H

#include <QDialog>
#include <QtSql>
#include <QSqlDatabase>

namespace Ui {
class TestWindow;
}

class TestWindow : public QDialog
{
    Q_OBJECT

public:
    explicit TestWindow(QWidget *parent = nullptr);
    ~TestWindow();

    float Result;
    QString ResultText;
    QTime EndTime;
    QMap<int, QString> Voprosi;
    QList<QPair<int, QString>> Answers;
    QList<QPair<QString, int>> Verno;
    QMap<int, QString>::Iterator CurrentVopros;
    QList<QString> TrueAnswers;

    void FillInfo(QString Question, QString one, QString two, QString three, QString four);
    void UpdateInfo(int Idvopr);
    void AddBalls(QVector<QString> Otvets);
    void SetOtchet();

private slots:
    void on_pushButton_clicked();

private:
    Ui::TestWindow *ui;

signals:
    void NextTest();
    void LastQuestion();
};

#endif // TESTWINDOW_H
