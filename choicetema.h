#ifndef CHOICETEMA_H
#define CHOICETEMA_H

#include "qdatetime.h"
#include <QDialog>

namespace Ui {
class ChoiceTema;
}

class ChoiceTema : public QDialog
{
    Q_OBJECT

public:
    explicit ChoiceTema(QWidget *parent = nullptr);
    ~ChoiceTema();

    void AddToBox(QString Text);

    int CurrentIndexTema;
    QTime StartTime;

private slots:
    void on_OK_btn_1_clicked();

private:
    Ui::ChoiceTema *ui;

signals:
    void GoToTest();
};

#endif // CHOICETEMA_H
