#include "registration.h"
#include "ui_registration.h"

Registration::Registration(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Registration)
{
    ui->setupUi(this);
}

Registration::~Registration()
{
    delete ui;
}

void Registration::on_pushButton_clicked()
{
    emit SignalShow();
    close();
}


void Registration::on_pushButton_2_clicked()
{
    emit SignalReg(ui->FirstName->text(), ui->SecondName->text(), ui->Telephone->text(), ui->login->text(), ui->password->text(), ui->DataBirthday->text());
    emit SignalShow();
    close();
}

