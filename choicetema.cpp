#include "choicetema.h"
#include "ui_choicetema.h"

ChoiceTema::ChoiceTema(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ChoiceTema)
{
    ui->setupUi(this);
}

ChoiceTema::~ChoiceTema()
{
    delete ui;
}

void ChoiceTema::AddToBox(QString Text)
{
    ui->comboBox->addItem(Text);
}

void ChoiceTema::on_OK_btn_1_clicked()
{
    CurrentIndexTema = ui->comboBox->currentIndex() + 1;
    emit GoToTest();
    StartTime = QTime::currentTime();
    close();
}

