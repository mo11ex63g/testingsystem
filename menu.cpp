#include "menu.h"
#include "qobjectdefs.h"
#include "ui_menu.h"

menu::menu(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::menu)
{
    ui->setupUi(this);
}

menu::~menu()
{
    delete ui;
}


void menu::on_pushButton_2_clicked()
{
    emit ShowUserInfo();
}


void menu::on_pushButton_4_clicked()
{
    close();
}

void menu::on_btn_tema_1_clicked()
{
    emit ShowQuestion();
}


void menu::on_pushButton_3_clicked()
{
    emit ShowOtchet();
}

