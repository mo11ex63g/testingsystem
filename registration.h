#ifndef REGISTRATION_H
#define REGISTRATION_H

#include <QDialog>

namespace Ui {
class Registration;
}

class Registration : public QDialog
{
    Q_OBJECT

public:
    explicit Registration(QWidget *parent = nullptr);
    ~Registration();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::Registration *ui;

signals:
    void SignalReg(QString RFirstName, QString RSecondName, QString RNumberPhone, QString RegLogin, QString RegPass, QString Date);
    void SignalShow();
};

#endif // REGISTRATION_H
