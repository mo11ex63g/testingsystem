#ifndef MENU_H
#define MENU_H

#include <QDialog>

namespace Ui {
class menu;
}

class menu : public QDialog
{
    Q_OBJECT

public:
    explicit menu(QWidget *parent = nullptr);
    ~menu();


private slots:
    void on_pushButton_2_clicked();
    void on_pushButton_4_clicked();
    void on_btn_tema_1_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::menu *ui;

signals:
    void ShowUserInfo();
    void ShowQuestion();
    void ShowOtchet();
};

#endif // MENU_H
