#include "userinfo.h"
#include "ui_userinfo.h"

userinfo::userinfo(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::userinfo)
{
    ui->setupUi(this);
}

userinfo::~userinfo()
{
    delete ui;
}

void userinfo::ShowInfo(QString First, QString Second, QString Phone, QString Date)
{
    ui->FirstName->setText(First);
    ui->SecondName->setText(Second);
    ui->PhoneNumber->setText(Phone);
    ui->DateBirthday->setText(Date);
}
