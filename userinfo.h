#ifndef USERINFO_H
#define USERINFO_H

#include <QDialog>

namespace Ui {
class userinfo;
}

class userinfo : public QDialog
{
    Q_OBJECT

public:
    explicit userinfo(QWidget *parent = nullptr);
    ~userinfo();

    void ShowInfo(QString First, QString Second, QString Phone, QString Date);
    void SetUserID(QString id) { User_ID = id; }
    QString GetUserID() { return User_ID; }

private:
    Ui::userinfo *ui;
    QString User_ID;

};

#endif // USERINFO_H
