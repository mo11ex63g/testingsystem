#ifndef OTCHET_H
#define OTCHET_H

#include <QDialog>

namespace Ui {
class Otchet;
}

class Otchet : public QDialog
{
    Q_OBJECT

public:
    explicit Otchet(QWidget *parent = nullptr);
    ~Otchet();

    void AddRow(QString Tema, QString ResultText, QString Ball, QString Data, QString Time);

private slots:
    void on_pushButton_clicked();

private:
    Ui::Otchet *ui;
};

#endif // OTCHET_H
