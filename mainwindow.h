#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "otchet.h"
#include <QMainWindow>
#include <registration.h>
#include <QtSql>
#include <QSqlDatabase>
#include <QMessageBox>
#include <QDebug>
#include <menu.h>
#include <userinfo.h>
#include <choicetema.h>
#include <testwindow.h>


QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_EnterButtom_clicked();
    void on_RegButton_clicked();

private:
    Ui::MainWindow *ui;
    Registration *RegUi;
    menu *MainMenu;
    userinfo *user;
    ChoiceTema *temaChoice;
    TestWindow *test;
    Otchet *otchet;

    QSqlDatabase database;

    QString FirstName = "none";
    QString SecondName = "none";
    QString NumberPhone = "none";
    QString DateBirthday = "none";

    bool bIsDo = false;
    bool bIsDo2 = false;
    bool Wait = true;

    void ConnectToBD();

public slots:
    void SlotReg(QString RFirstName, QString RSecondName, QString RNumberPhone, QString RegLogin, QString RegPass, QString Date);
    void SlotShow();
    void SlotShowUserInfo();
    void SlotShowQuestion();
    void SlotTest();
    void SlotNextQuest();
    void SlotShowOtchet();
    void SlotSetOtchet();
};
#endif // MAINWINDOW_H
